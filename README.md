# TicTacToe
user-defined TicTacToe: gamefield size, players, win length

### jQuery widget TicTacToe by ondrab - Options
__size__ is gamefield size
```
size: 8
```

__winLength__ is number of characters in a row to win
```
winLength: 8
```

__players__ number of playing players
```
players: 2
```

__playerChars__ is object of player characters
```
playerChars: { 1: 'X', 2: 'O',3: 'W', 4: 'U', 5: 'H', 6: 'C' }
```

__playerColors__ is object of player colors
```
playerColors: { 1: 'black', 2: 'green', 3: 'red', 4: 'blue', 5: 'pink', 6: 'yellow'}
```

__ajaxScriptLocation__ location for ajax script where data are sent
```
ajaxScriptLocation: './ajax_evaluation.php'
```
