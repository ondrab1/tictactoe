<?php 



class Evaluation
{

	protected $field = [];

	protected $winLength;


	protected $counter = 0;
	protected $lastPlayer = null;

	public function __construct(array $field, int $winLength)
	{
		$this->field = $this->fixField($field);
		$this->winLength = $winLength;
	}

	public function getWinner()
	{
		return $this->findWinner();
	}

	public function getField()
	{
		return $this->field;
	}

	public function getSize()
	{
		return count($this->getField());
	}

	public function fixField($field)
	{
		$newField = [];
		foreach ($field as $rowKey => $row) {
			foreach ($row as $columnKey => $value) {
				if (empty($value)) {
					$value = null;
				}
				$newField[$rowKey][$columnKey] = $value;
			}
		}

		return $newField;
	}

	private function findWinner()
	{
		if (($check = $this->checkRow()) !== null) {
			return $check;
		}

		if (($check = $this->checkColumn()) !== null) {
			return $check;
		}

		if (($check = $this->checkDiagonal_1()) !== null) {
			return $check;
		}

		if (($check = $this->checkDiagonal_2()) !== null) {
			return $check;
		}

		if (($check = $this->checkDraw()) !== null) {
			return $check;
		}

		return null;
	}

	private function checkRow()
	{
		$this->reset();

		foreach ($this->getField() as $row) {
			foreach ($row as $value) {
				if (($check = $this->valueCheck($value)) !== false) {
					return $check;
				}
			}
			$this->reset();
		}

		return null;
	}

	private function checkColumn()
	{
		$this->reset();

		for ($i = 1; $i <= $this->getSize(); $i++) {
			foreach ($this->getField() as $row) {
				if (($check = $this->valueCheck($row[$i - 1])) !== false) {
					return $check;
				}
			}
			$this->reset();
		}

		return null;
	}

	private function checkDiagonal_1()
	{
		$this->reset();

		$field = $this->getField();

		for ($valueIndex = $this->getSize(); $valueIndex >= 1; $valueIndex--) {
			$vIndex = $valueIndex - 2;
			for ($rowIndex = 1; $rowIndex <= $this->getSize(); $rowIndex++) {
				if (array_key_exists($vIndex + $rowIndex, $field[$rowIndex])) {
					if (($check = $this->valueCheck($field[$rowIndex][$vIndex + $rowIndex])) !== false) {
						return $check;
					}
				}
			}
			$this->reset();
		}

		for ($valueIndex = $this->getSize(); $valueIndex >= 1; $valueIndex--) {
			$vIndex = $valueIndex - 2;
			for ($rowIndex = $this->getSize(); $rowIndex >= 1; $rowIndex--) {
				if (array_key_exists($vIndex + $rowIndex, $field[$rowIndex])) {
					if (($check = $this->valueCheck($field[$rowIndex][$vIndex + $rowIndex])) !== false) {
						return $check;
					}
				}
			}
			$this->reset();
		}


		return null;
	}

	private function checkDiagonal_2()
	{
		$this->reset();

		$field = $this->getField();

		for ($valueIndex = 1; $valueIndex <= $this->getSize(); $valueIndex++) {
			$vIndex = $valueIndex;
			for ($rowIndex = 1; $rowIndex <= $this->getSize(); $rowIndex++) {
				if (array_key_exists($vIndex - $rowIndex, $field[$rowIndex])) {
					if (($check = $this->valueCheck($field[$rowIndex][$vIndex - $rowIndex])) !== false) {
						return $check;
					}
				}
			}
			$this->reset();
		}

		for ($valueIndex = 1; $valueIndex <= $this->getSize(); $valueIndex++) {
			$vIndex = $valueIndex;
			for ($rowIndex = $this->getSize(); $rowIndex >= 1; $rowIndex--) {
				if (array_key_exists($vIndex - ($rowIndex - 2), $field[$rowIndex])) {
					if (($check = $this->valueCheck($field[$rowIndex][$vIndex - ($rowIndex - 2)])) !== false) {
						return $check;
					}
				}
			}
			$this->reset();
		}


	}

	private function checkDraw()
	{
		$emptyCounter = pow($this->getSize(), 2);
		foreach ($this->getField() as $row) {
			foreach ($row as $value) {
				if ($value !== null) {
					$emptyCounter--;
				}	
			}
		}

		if ($emptyCounter == 0) {
			return -1;
		}

		return null;
	}

	private function valueCheck($value)
	{
		if ($value !== null && is_numeric($value)) {
			if ($this->counter > 0 && $this->lastPlayer != $value) {
				$this->counter = 0;
			}
			$this->counter++;
			$this->lastPlayer = $value;

			if ($this->checkWin()) {
				return $this->lastPlayer;
			}
		} else if ($value === null) {
			$this->reset();
		}

		return false;
	}

	private function reset()
	{
		$this->lastPlayer = null;
		$this->counter = 0;
	}

	private function checkWin()
	{
		if ($this->counter == $this->winLength) {
			return true;
		}

		return false;
	}


}



?>