$.widget("ondrab.TicTacToe", {

	options: {
		size: 3,
		winLength: 3,
		players: 2,
		playerChars: { 1: 'X', 2: 'O',3: 'W', 4: 'U', 5: 'H', 6: 'C' },
		playerColors: { 1: 'black', 2: 'green', 3: 'red', 4: 'blue', 5: 'pink', 6: 'yellow'},
		ajaxScriptLocation: './ajax_evaluation.php'
	},

	currentPlayer: 1,
	maxPlayers: 6,
	gameEnd: false,

    _create: function() {
        this.renderField();

        if (this.getPlayers() > this.maxPlayers) {
    		alert('Limit of players was exceeded!');
    		this._destroy();
    	}

        var me = this;
        
        $('[data-field]').on('click', '[data-column]', function (e) {
        	if (me.gameEnd == false) {
        		me.click($(this));
        	}
        });
    },

    _destroy: function () {
    	$('[data-field]').text('');
    },

    click: function ($item) {
 		var value = $item.text();

 		if (value == '') {
 			$item.data('player', this.currentPlayer);
    		$item.text(this.getPlayerChar());
    		$item.css('color', this.getPlayerColor());

    		this.checkPlayer();

    		this.check();
 		}
    },

    check: function () {
    	var me = this;

    	$.ajax({
		  method: "GET",
		  url: this.options.ajaxScriptLocation,
		  data: { field: this.getField(), size: this.getWinLength() }
		})
		  .done(function(data) {
		  	if (data != '' && data != undefined) {
		  		me.gameEnd = true;

		  		var status;
		  		if (data == -1) {
		  			status = 'draw';
				} else {
					status = me.getPlayerChar(data);
				}

		  		me.afterEnd(null, status);
		  	}
		  });
    },

    afterEnd: function (e, status) {
    	var me = this;

    	if (this._trigger('afterEnd', e, status)) {
			if (status == 'draw') {
			  	alert('Draw');
			 } else {
			  	alert(status + ' is winner');
			 }
		}
    },


    getPlayerChar: function (player = null) {
    	if (player != null) {
    		return this.options.playerChars[player];
    	}

    	return this.options.playerChars[this.currentPlayer];
    },

    getPlayerColor: function () {
    	return this.options.playerColors[this.currentPlayer];
    },

    getActualPlayer: function () {
    	return chars[this.currentPlayer];
    },

    checkPlayer: function () {
    	if (++this.currentPlayer > this.getPlayers()) {
    		this.currentPlayer = 1;
    	}
    },

    getField: function () {
		var field = {};
		$('[data-row]').each(function() {
			var row = $(this).data('row');
			field[row] = {};

			$(this).find('[data-column]').each(function() {
				var column = $(this).data('column');
				var value = $(this).data('player');

				if (value == '') {
					value = null;
				}

				field[row][column] = value;
			});
		});

		return field;
	},

	renderField: function () {
		var field = $('[data-field]');

		for (i = 1; i <= this.getSize(); i++) { 
			field.append('<tr data-row="' + i + '"></tr>');
		}

		var me = this;

		field.find('[data-row]').each(function () {
			for (i = 1; i <= me.getSize(); i++) { 
				$(this).append('<td data-column="' + (i - 1) + '" data-player=""></td>');
			}
		});
	},

	getSize: function () {
		return this.options.size;
	},

	getPlayers: function () {
		return this.options.players;
	},

	getWinLength: function () {
		return this.options.winLength;
	}
});